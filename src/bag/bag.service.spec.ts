import { Test, TestingModule } from '@nestjs/testing';
import { BagService } from './bag.service';
import { RequestHelper } from '../fabric/core/chain-interface/requesthelper';
import { BagData } from 'src/bag-schema/BagData';

describe('BagService', () => {
  let service: BagService;
  const requestHelperMock = { queryRequest: jest.fn(), invokeRequest: jest.fn() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BagService,
        { provide: RequestHelper, useValue: requestHelperMock },
      ],
    }).compile();

    service = module.get<BagService>(BagService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getBag', () => {
    it('should call request helper query', async () => {
      requestHelperMock.queryRequest.mockReturnValue(new Promise((bag) => ({ bagTag: '1234567890' }) as BagData));

      service.getBag({ bagKey: '012312321312', bagDate: '2019-08-08' });

      expect(requestHelperMock.queryRequest).toHaveBeenCalledWith('getBag', ['2019-08-08012312321312']);
    });

    it('should return bag when bagInfo found matching supplied bag tag', async () => {
      requestHelperMock.queryRequest.mockReturnValue(new Promise((resolve, reject) => { resolve({ bagTag: '1234567890' } as BagData); }));

      const result = await service.getBag({ bagKey: '1234567890', bagDate: '2019-08-08' });

      expect(result.bagTag).toEqual('1234567890');
    });

    it('should return 404 when bagInfo not found matching supplied bag tag', async () => {
      requestHelperMock.queryRequest.mockReturnValue(new Promise((resolve, reject) => { resolve(null); }));

      try {
        const result = await service.getBag({ bagKey: '012312321312', bagDate: '2019-08-08' });
        fail('unexpected sucess');
      } catch (err) {
        expect(err.response.message).toEqual(`Bag does not exist with bagKey: null`);
      }
    });

    it('should throw an error when the requestHelper fails', async () => {
      requestHelperMock.queryRequest.mockReturnValue(new Promise((resolve, reject) => { reject('something went wrong'); }));

      try {
        const result = await service.getBag({ bagKey: '012312321312', bagDate: '2019-08-08' });
        fail('unexpected sucess');
      } catch (err) {
        expect(err.response.message).toEqual('something went wrong');
      }
    });
  });

  describe('createBag', () => {
    it('should call invoke helper query', async () => {
      requestHelperMock.invokeRequest.mockReturnValue(new Promise((bag) => ({
        bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' },
      })));

      service.createBag({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });

      expect(requestHelperMock.invokeRequest).toHaveBeenCalledWith(
        'saveBag', ['{\"bagTag\":\"1234567890\",\"locationInfo\":{\"description\":\"Dublin\",\"geocoordinates\":\"12NW2.78E21\"}}']);
    });
  });

  it('should save bag when BagInfo is provided', async () => {
    requestHelperMock.invokeRequest.mockReturnValue(new Promise((resolve, reject) => {
      resolve({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });
    }));

    const result = await service.createBag({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });

    expect(result).toEqual({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });

  });

  it('should throw error  when BagInfo is not provided', async () => {
    requestHelperMock.invokeRequest.mockReturnValue(new Promise((resolve, reject) => { reject('createBag failed'); }));

    try {
      const result = await service.createBag({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });
      fail('unexpected sucess');
    } catch (err) {
      expect(err.response.message).toEqual('createBag failed');
    }

  });
});
