import {
  Body, Controller, Get, HttpCode, HttpException, HttpStatus,
  NotFoundException, Param, Patch, Post, Put, Req, UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags, ApiModelProperty, ApiBearerAuth, ApiImplicitBody } from '@nestjs/swagger';
import { BagService, BagChainMethod } from './bag.service';
import { BagData } from 'src/bag-schema/BagData';
import { BagDataHistory } from 'src/bag-schema/BagDataHistory';
import { BagRequestData } from 'src/bag-schema/BagRequestData';
import { SuccessResponse } from 'src/bag-schema/SuccessResponse';
import { FailureResponse } from 'src/bag-schema/FailureResponse';
import { thisExpression } from '@babel/types';

@ApiUseTags('bag')
@UseGuards(AuthGuard('jwt'))
@Controller('bag')
export class BagController {
  constructor(private readonly bagService: BagService) { }

  // ***POST A BAG***
  @ApiOperation({
    description: 'When creating a bag with the POST command, the following fields\
     are mandatory: bagTag and locationInfo.description. E.g bagTag: 01234567890, locationInfo: {"description": "Dublin"}.\
     If no departure date is specified, the date will default to todays date. E.g 2019-10-08.',
    title: 'Create a new bag',
  })

  @ApiResponse({ description: 'The bag has been successfully created.', status: 200, type: SuccessResponse })
  @ApiResponse({ description: 'The bag has been successfully created.', status: 201, type: SuccessResponse })
  @ApiResponse({ description: 'The input bag data is invalid.', status: 400, type: FailureResponse })
  @ApiResponse({ description: 'The input bag data is invalid.', status: 500, type: FailureResponse })

  @ApiBearerAuth()
  @Post()
  public async createBag(
    @Body() bag: BagData) {
    return await this.bagService.createBag(bag);
  }

  // ***GET A BAG HISTORY***
  @ApiOperation({
    description: 'Returns the history of a bag included with a timestamp identified by bagDate and bagKey',
    title: 'Get a bags history',
  })
  @ApiImplicitParam({
    description: 'The bag date and a unique key for each bag. The key is made up of 10 digits\
       e.g. 2019-09-22/01234567890/history',
    name: 'Bag History',
    required: true,
    type: 'string',
  })
  @ApiBearerAuth()
  @Get('/:bagDate/:bagKey/history')
  @ApiResponse({ description: 'The bag history has been successfully returned.', status: 200, type: SuccessResponse })
  @ApiResponse({
    description: 'No bag history matching the given date and key has been found.', status: 404,
    type: FailureResponse,
  })
  public async getBagHistory(@Param() params: BagRequestData): Promise<BagDataHistory[]> {
    return this.bagService.getBagHistory(params);
  }

  // ***GET A BAG***
  @ApiOperation({
    description: 'Returns a bag identified by bagDate and bagKey',
    title: 'Get a bag',
  })
  @ApiImplicitParam({
    description: 'The bag date and a unique key for each bag. The key is made up of 10 digits\
       e.g. 2019-09-22/01234567890',
    name: 'bag',
    required: false,
    type: 'string',
  })
  @ApiBearerAuth()
  @Get('/:bagDate/:bagKey')
  @ApiResponse({ description: 'The bag has been successfully returned.', status: 200, type: SuccessResponse, isArray: true })
  @ApiResponse({
    description: 'No bag matching the given date and key has been found.', status: 404,
    type: FailureResponse,
  })
  public async getBag(@Param() params: BagRequestData): Promise<BagData> {
    if (!params) {
      throw new Error('missing bagKey in request');
    }
    return await this.bagService.getBag(params);
  }

  // ***GET BAG WITH QUERY***
  @ApiOperation({
    description: 'Returns the list of bags matching supplied query',
    title: 'Get bags matching query',
  })
  @ApiBearerAuth()
  @Post('/find')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    description: 'The list of bags have been successfully returned.', isArray: true,
    status: 200, type: BagData,
  })
  @ApiResponse({ status: 404, description: 'No bag matching the query has been found.' })
  public async getBagQuery(
    @Body() query: any): Promise<BagData[]> {
    return this.bagService.getBagQuery(query);
  }

  @ApiBearerAuth()
  @Get('events')
  public async getEventList() {
    return this.bagService.getEventList();
  }
}
