import { Test, TestingModule } from '@nestjs/testing';
import { BagController } from './bag.controller';
import { BagService } from './bag.service';

describe('Bag Controller', () => {
  let bagController: BagController;
  const bagServiceMock = { getBag: jest.fn(), createBag: jest.fn() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BagController],
      providers: [{ provide: BagService, useValue: bagServiceMock }],
    }).compile();

    bagController = module.get<BagController>(BagController);
  });

  it('should be defined', () => {
    expect(bagController).toBeDefined();
  });

  describe('createBag', () => {

    it('should create bag when BagInfo is provided', async () => {
      bagServiceMock.createBag.mockReturnValue(new Promise((resolve, reject) => {
        resolve({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });
      }));

      const result = await bagController.createBag({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });

      expect(result).toEqual({ bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' } });

    });

    it('should throw error  when BagInfo is not provided', async () => {
      bagServiceMock.createBag.mockReturnValue(new Promise((resolve, reject) => { reject('createBag failed'); }));
      try {
        const result = await bagController.createBag({
          bagTag: '1234567890', locationInfo: { description: 'Dublin', geocoordinates: '12NW2.78E21' },
        });
        fail('unexpected sucess');
      } catch (err) {
        expect(err).toEqual('createBag failed');
      }

    });
  });
  describe('getBag', () => {

    it('should throw error when param is null', async () => {
      bagServiceMock.getBag.mockReturnValue(new Promise((resolve, reject) => { resolve(null); }));
      try {
        await bagController.getBag(null);
        fail('unexpected sucess');
      } catch (err) {
        expect(err.message).toEqual('missing bagKey in request');
      }
    });
  });

});
