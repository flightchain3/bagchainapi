import { Injectable, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { RequestHelper } from '../../src/fabric/core/chain-interface/requesthelper';
import { BagData } from 'src/bag-schema/BagData';
import { BagDataHistory } from 'src/bag-schema/BagDataHistory';
import { BagRequestData } from 'src/bag-schema/BagRequestData';
import { SuccessResponse } from 'src/bag-schema/SuccessResponse';
import { FailureResponse } from 'src/bag-schema/FailureResponse';

export enum BagChainMethod {
    'getVersion' = 'getVersion',
    'getBag' = 'getBag',
    'saveBag' = 'saveBag',
    'getBagHistory' = 'getBagHistory',
    'getBagQuery' = 'getBagQuery',
}

@Injectable()
export class BagService {

    constructor(private requestHelper: RequestHelper) {
    }

    public getVersion(): Promise<any> {
        return this.requestHelper.queryRequest(BagChainMethod.getVersion, []);
    }

    public async getBag(bag: BagRequestData): Promise<BagData> {
        const key = bag.bagDate + bag.bagKey;
        return this.requestHelper.queryRequest(BagChainMethod.getBag, [key]).then(
            (bag) => {
                if (!bag) {
                    throw new NotFoundException(`Bag does not exist with bagKey: ${bag}`);
                }
                return bag as BagData;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );
    }

    public async getBagQuery(query: any): Promise<BagData[]> {
        return this.requestHelper.queryRequest(BagChainMethod.getBagQuery, [JSON.stringify(query)]).then(
            (bags) => {
                if (!bags) {
                    throw new NotFoundException(`no bags exist match the query: ${query}`);
                }
                return bags;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );
    }

    public async getBagHistory(bag: BagRequestData): Promise<BagDataHistory[]> {
        const key = bag.bagDate + bag.bagKey;

        return this.requestHelper.queryRequest(BagChainMethod.getBagHistory, [key]).then(
            (bagHistory) => {
                if (!bagHistory || bagHistory.length === 0) {
                    throw new NotFoundException(`Bag History does not exist for key: ${key}`);
                }
                const bagHistoryResponse: BagDataHistory[] = [];
                for (const item of bagHistory) {
                    bagHistoryResponse.push({
                        bagData: item.bagData,
                        timestamp: new Date(item.timestamp.seconds.low * 1000),
                    });
                }
                return bagHistoryResponse;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );
    }

    public async createBag(bag: BagData): Promise<any> {
        // seperate private data from BagData object
        const transientMap = new Map<string, Buffer>();
        if (bag.passengers) {
            transientMap[`passengers`] = Buffer.from(JSON.stringify(bag));
            bag.passengers = null;
        }

        return this.requestHelper.invokeRequest(BagChainMethod.saveBag, [JSON.stringify(bag)], transientMap)
            .then(
                (response) => {
                    const sr = new SuccessResponse();
                    return sr;
                }, (error) => {
                    // TODO what comes back
                    const sr = new FailureResponse();
                    throw new InternalServerErrorException(sr);
                });
    }

    public async getEventList() {
        const bagEvents = require('../assets/data/events.json');

        return bagEvents;
    }
}
