import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BagController } from './bag/bag.controller';
import { BagService } from './bag/bag.service';
import { EnvConfig } from './fabric/common/config/env';
import { CoreModule } from './fabric/core/core.module';
import { ChainModule } from './fabric/core/chain-interface/chain.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './auth/user/user.module';
import { User } from './auth/user/user.entity';
import { HealthController } from './health/health.controller';

@Module({

  imports: [TypeOrmModule.forRoot(AppModule.getDBSettings()), CoreModule, ChainModule, AuthModule, UserModule, User],
  controllers: [AppController, BagController, HealthController],
  providers: [AppService, BagService],
})

export class AppModule {
  private static getDBSettings(): any {
    console.log('getting db settings');
    console.log('connecting to db at "' + EnvConfig.USER_DB_HOST + '"');
    return {
      database: EnvConfig.USER_DB_NAME,
      entities: ['src/**/**.entity{.ts,.js}'],
      host: EnvConfig.USER_DB_HOST,
      password: EnvConfig.USER_DB_PASSWORD,
      port: 3306,
      synchronize: true,
      type: 'mysql',
      username: EnvConfig.USER_DB_USERNAME,
    };
  }
}
