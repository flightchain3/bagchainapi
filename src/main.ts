import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // tslint:disable-next-line: max-line-length
  const description = '<p>This Bag Chain API is used to interface to the Smart Contract.Publishers (airlines or airports) will POST to create a new bag record, and can then GET all bag history.</p>' +
  // tslint:disable-next-line: max-line-length
          '<p><h3>API Authorisation</h3>To call the API, you must be issued a token. This key is unique to each organisation. The token will be passed in via an Authorization: Bearer HTTP header, such as -H "Authorization: Bearer TOKEN"</p>' +
  // tslint:disable-next-line: max-line-length
          '<p><h3>Bag Key</h3>Each bag is uniquely identified by the date and a bag key.  The bag key consists of a 10 digit bagTag number. e.g. 2019-08-23/0123456789. When a bag is ' +
  // tslint:disable-next-line: max-line-length
          'created, it is stored in the blockchain state using the unique date of operation and Bag Key for that bag and you can subsequently use that retrieve the bag data.</p>';

  const options = new DocumentBuilder()
    .setTitle('Bag Chain REST API for BagChainSAA')
    .setDescription(description)
    .setVersion('1.0')
    .addBearerAuth('Authorization', 'header')
    .addTag('BagChain')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-doc', app, document);

  app.enableCors();
  await app.listen(3000);
}
bootstrap();
