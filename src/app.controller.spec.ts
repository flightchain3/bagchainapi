import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RequestHelper } from './fabric/core/chain-interface/requesthelper';

describe('AppController', () => {
  let appController: AppController;
  const requestHelperMock = { queryRequest: jest.fn(), invokeRequest: jest.fn() };

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService,
        { provide: RequestHelper, useValue: requestHelperMock }],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "This is the BagChain API"', () => {
      expect(appController.getHello()).toBe('This is the BagChain API');
    });
  });
});
