import { Injectable } from '@nestjs/common';
import { RequestHelper } from './fabric/core/chain-interface/requesthelper';

export enum BagChainMethod {
  'getVersion' = 'getVersion',
}

@Injectable()
export class AppService {

  constructor(private requestHelper: RequestHelper) {
  }

  public getVersion(): Promise<any> {
    return this.requestHelper.queryRequest(BagChainMethod.getVersion, []);
  }

  getHello(): string {
    return 'This is the BagChain API';
  }
}
