import { ApiModelProperty } from '@nestjs/swagger';

export class UserData {
    @ApiModelProperty({ required: false })
    public type?: string;
    @ApiModelProperty({ required: false })
    public fullName?: string;
    @ApiModelProperty({ required: false })
    public company?: string;
}
