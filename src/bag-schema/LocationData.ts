import { ApiModelProperty } from '@nestjs/swagger';

export class LocationData  {
    @ApiModelProperty({ required: true })
    public description: string;
    @ApiModelProperty({ required: false })
    public geocoordinates?: string;
}
