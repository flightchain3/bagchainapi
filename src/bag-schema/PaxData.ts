import { ApiModelProperty } from '@nestjs/swagger';

export class PaxData {
    @ApiModelProperty({ required: false })
    public fullname?: string;
    @ApiModelProperty({ required: false })
    public seatNumber?: string;
    @ApiModelProperty({ required: false })
    public status?: string;
    @ApiModelProperty({ required: false })
    public seqNum?: string;
    @ApiModelProperty({required: false})
    public signature?: string;
}
