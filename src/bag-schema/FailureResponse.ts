import { ApiModelProperty } from '@nestjs/swagger';

export class FailureResponse {

    constructor() {
        this.message = 'bagtag added';
        this.success = false;
    }

    @ApiModelProperty({required: false, example: 'bagtag added'})
    public message?: string;
    @ApiModelProperty({required: false, example: 'false'})
    public success?: boolean;
}
