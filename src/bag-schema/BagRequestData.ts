import { ApiModelProperty } from '@nestjs/swagger';

export class  BagRequestData {
    @ApiModelProperty({ required: true })
    public bagDate: string;
    @ApiModelProperty({ required: true })
    public bagKey: string;
}
