import { ApiModelProperty } from '@nestjs/swagger';

export class ActionData {
    @ApiModelProperty({ required: false })
    public actionName?: string;
}
