import { ApiModelProperty } from '@nestjs/swagger';
import { LocationData } from './LocationData';
import { FlightData } from './FlightData';
import { PaxData } from './PaxData';
import { EventData } from './EventData';
import { ActionData } from './ActionData';
import { UserData } from './UserData';

export class BagData {
    @ApiModelProperty({ required: true })
    public bagTag: string;
    @ApiModelProperty({ required: true })
    public locationInfo: LocationData;
    @ApiModelProperty({ required: false })
    public operatorID?: string;
    @ApiModelProperty({ required: false })
    public uldID?: string;
    @ApiModelProperty({ required: false })
    public baggageCarousel?: string;
    @ApiModelProperty({ required: false })
    public flight?: FlightData;
    @ApiModelProperty({ type: [PaxData], required: false })
    public passengers?: PaxData[];
    @ApiModelProperty({ required: false })
    public events?: EventData;
    @ApiModelProperty({ required: false })
    public actions?: ActionData;
    @ApiModelProperty({ required: false })
    public user?: UserData;

}
