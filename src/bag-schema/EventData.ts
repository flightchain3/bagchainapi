import { ApiModelProperty } from '@nestjs/swagger';

export class EventData {
    @ApiModelProperty({ required: false })
    public eventName?: string;
}
