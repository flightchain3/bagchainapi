import { ApiModelProperty } from '@nestjs/swagger';

export class SuccessResponse {

    constructor() {
        this.message = 'bagtag added';
        this.success = true;
    }

    @ApiModelProperty({ required: false, example: 'bagtag added' })
    public message?: string;
    @ApiModelProperty({ required: false, example: 'true' })
    public success?: boolean;
}
