import {ApiModelProperty} from '@nestjs/swagger';
import { BagData } from './BagData';

export class BagDataHistory {
    @ApiModelProperty({required: true})
    public bagData: BagData;
    @ApiModelProperty({required: true})
    public timestamp: Date;

}
