import { ApiModelProperty } from '@nestjs/swagger';

export class FlightData {
    @ApiModelProperty({ required: false })
    public carrierCode?: string;
    @ApiModelProperty({ required: false })
    public flightNumber?: string;
    @ApiModelProperty({ required: true })
    public departureDate: string;
    @ApiModelProperty({ required: false })
    public departureTime?: string;
    @ApiModelProperty({ required: false })
    public departureAirport?: string;
    @ApiModelProperty({ required: false })
    public destinationAirport?: string;

}
