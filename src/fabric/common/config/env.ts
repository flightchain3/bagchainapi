import { config } from 'dotenv';
import * as path from 'path';
import { Log } from '../utils/logging/log.service';

/**
 * node EnvConfig variables,
 * copy .env.example file, rename to .env
 *
 * @export
 * @class EnvConfig
 */
export class EnvConfig {

    public static LISTENING_PORT_STRING = 'LISTENING_PORT';
    public static NODE_ENV_STRING = 'NODE_ENV';
    public static IDENTITY_STRING = 'IDENTITY';
    public static HFC_STORE_PATH_STRING = 'HFC_STORE_PATH';
    public static CHANNEL_STRING = 'CHANNEL';
    public static MSPID_STRING = 'MSPID';
    public static IS_DEMO_MODE_STRING = 'IS_DEMO_MODE';
    public static USE_SITA_TEST_FABRIC_NETWORK_STRING = 'USE_SITA_TEST_FABRIC_NETWORK';
    public static USER_TOKENS_STRING = 'USER_TOKENS';
    public static USER_DB_HOST_STRING = 'USER_DB_HOST';
    public static USER_DB_NAME_STRING = 'USER_DB_NAME';
    public static USER_DB_USERNAME_STRING = 'USER_DB_USERNAME';
    public static USER_DB_PASSWORD_STRING = 'USER_DB_PASSWORD';
    public static FABRIC_NETWORK_STRING = 'FABRIC_NETWORK';

    // NODE
    public static LISTENING_PORT = process.env[EnvConfig.LISTENING_PORT_STRING] || 3000;
    public static NODE_ENV = process.env[EnvConfig.NODE_ENV_STRING] || 'LOCAL';

    // FABRIC
    public static IDENTITY = process.env[EnvConfig.IDENTITY_STRING];
    public static HFC_STORE_PATH = process.env[EnvConfig.HFC_STORE_PATH_STRING]
        || '/home/osboxes/Flightchain3/FlightChainAPI/bootstrap/hfc-key-store';
    //        || path.join('./', '/usr/src/app/bootstrap/hfc-key-store');
    public static CHANNEL = process.env[EnvConfig.CHANNEL_STRING] || 'channel-flight-chain';

    // Organisational MSP Id
    public static MSPID: string = process.env[EnvConfig.MSPID_STRING] || 'SITAMSP';

    // True if this app is in demo mode, and not connected to a fabric network.
    public static IS_DEMO_MODE = process.env[EnvConfig.IS_DEMO_MODE_STRING] || false;

    public static USE_SITA_TEST_FABRIC_NETWORK = process.env[EnvConfig.USE_SITA_TEST_FABRIC_NETWORK_STRING] || false;

    public static FABRIC_NETWORK = process.env[EnvConfig.FABRIC_NETWORK_STRING] || 'local';

    public static USER_TOKENS = process.env[EnvConfig.USER_TOKENS_STRING];

    public static USER_DB_HOST = process.env[EnvConfig.USER_DB_HOST_STRING]
        || 'sandboxdb-demo.crxmenrr0jky.us-east-1.rds.amazonaws.com';
    public static USER_DB_NAME = process.env[EnvConfig.USER_DB_NAME_STRING] || 'bagchaindb';
    public static USER_DB_USERNAME = process.env[EnvConfig.USER_DB_USERNAME_STRING] || 'bagchainUser';
    public static USER_DB_PASSWORD = process.env[EnvConfig.USER_DB_PASSWORD_STRING] || 'S1taBagChain';

    public static initialise() {
        // list env keys in console
        Log.config.debug('Initialising the environment variables.');

        for (const propName of Object.keys(EnvConfig)) {
            Log.config.debug(`${propName}:  ${EnvConfig[propName]}`);
        }
    }
}
