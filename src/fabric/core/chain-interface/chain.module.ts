import { Module } from '@nestjs/common';
import { HlfClient } from './hlfclient';
import { HlfConfig } from './hlfconfig';
import { RequestHelper } from './requesthelper';

@Module({
    exports: [
        RequestHelper,
        HlfConfig,
        HlfClient,
    ],
    imports: [
        // QueueModule,
        // EventsModule
    ],
    providers: [
        RequestHelper,
        HlfConfig,
        HlfClient,
    ],
})
export class ChainModule {
}
