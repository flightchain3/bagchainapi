import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()

export class User {

  @PrimaryGeneratedColumn()

  public id: number;

  @Column({ length: 100, unique: true })

  public username: string;

  @Column({ length: 255 })

  public password: string | undefined;

  @Column({ length: 50 })

  public iATACode: string | undefined;

  @Column({ length: 255 })

  public fullName: string | undefined;

  @Column({ length: 255 })

  public type: string | undefined;

  @Column()

  public flag: boolean | undefined;

  @Column()

  public isActive: boolean | undefined;

  @Column({ length: 255 })

  public location: string | undefined;

  @Column({ length: 255 })

  public geoCoordinates: string | undefined;

  @Column({ length: 255 })

  public event: string | undefined;

  @Column({ length: 255 })

  public company: string | undefined;

}
