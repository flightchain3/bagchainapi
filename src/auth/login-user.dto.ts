import { ApiModelProperty } from '@nestjs/swagger';
export class LoginUserDto {
@ApiModelProperty()
 public readonly username: string;
 @ApiModelProperty()
 public readonly password: string;
}
