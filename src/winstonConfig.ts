import * as winston from 'winston';

const { combine, timestamp, label, printf } = winston.format;

export class WinstonLogger {
    public logger: winston.Logger;

    private options = {
        console: {
            colorize: true,
            handleExceptions: true,
            json: false,
            level: 'debug',
        },
    };

    private myFormat = printf((info) => {
        return `[${info.level}]  ${info.message}`;
    });

    constructor() {
        this.logger = winston.createLogger({
            format: this.myFormat,
            level: 'info',
            transports: [
                new winston.transports.Console(this.options.console),
            ],
        });
    }
}
