import { Controller, Get, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('version')
  public async getVersion(
    @Req() request): Promise<string> {
    return this.appService.getVersion();
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
