import { Module } from '@nestjs/common';
import { HealthController } from './health.controller';
import { BagService } from 'src/bag/bag.service';

@Module({
    controllers: [HealthController],
    imports: [BagService],
})
export class HealthModule {
}
