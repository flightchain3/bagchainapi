set -e # Turn error checking back on

echo $CERT_DATA

echo "Environment Variable Sanity Check...."
if [ -z "$CERT_DATA" ]; then echo "ERROR: CERT_DATA is not set or is blank";  exit 1; else echo "CERT_DATA is set to '$CERT_DATA'"; fi
if [ -z "$PUBLIC_KEY" ]; then echo "ERROR: PUBLIC_KEY is not set or is blank";  exit 1; else echo "PUBLIC_KEY is set to '$PUBLIC_KEY'"; fi
if [ -z "$PRIVATE_KEY" ]; then echo "ERROR: PRIVATE_KEY is not set or is blank";  exit 1; else echo "PRIVATE_KEY is set to '$PRIVATE_KEY'"; fi
if [ -z "$SIGNING_IDENTITY" ]; then echo "ERROR: SIGNING_IDENTITY is not set or is blank";  exit 1; else echo "SIGNING_IDENTITY is set to '$SIGNING_IDENTITY'"; fi

#Creates namespace or ignores if already there
## Using funky two stage create then apply throughout script to create if new and ignore or update if exists.
kubectl create namespace "adapters" -o yaml --dry-run | kubectl apply -f -
echo "namespace created"

#Needed so k8 can authenticate when pulling docker image of deployment.yml
# This might be better done as a one time activity to prevent dealing with tokena
kubectl create secret docker-registry "bag-chain-api" --docker-server="registry.gitlab.com/flightchain3/bagchainapi" --docker-username=$GITLAB_USERNAME --docker-password=$GITLAB_PASSWORD --docker-email=$GITLAB_EMAIL -o yaml --dry-run | kubectl apply --namespace="adapters" -f -

# Setup certificates for this airline
# These values are from the 'registerUser.js' script, and stored in the project runner settings as secrets
# We need to have a separate config map for each identity.
# This is referenced in deployment.yaml file.

mkdir hfc-key-store
echo $CERT_DATA > hfc-key-store/$IDENTITY
echo $PUBLIC_KEY > hfc-key-store/$SIGNING_IDENTITY-pub
echo $PRIVATE_KEY > hfc-key-store/$SIGNING_IDENTITY-priv
ls -al hfc-key-store

kubectl create configmap fabric-api-cert-config-"bchain-api" --from-file=hfc-key-store -o yaml --dry-run | kubectl apply --namespace="adapters" -f -
echo "config map set"

# Setup telegraf configuration
# mv monitoring/telegraf.toml telegraf.conf  #rename to match default config name
# kubectl create configmap telegraf-config --from-file=telegraf.conf -o yaml --dry-run | kubectl apply --namespace="bag-chain-api" -f -

#For visual inspection of substitutions
cat k8s-deployments/*.yml

#apply all of the yamls in deployment folder
kubectl apply -f k8s-deployments --namespace="adapters"
echo "deployed"
